import 'package:flutter/material.dart';

class TextWidget extends StatelessWidget {
  const TextWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Apps Hellow"),
        ),
        body: Center(
          child: Container(
            color: Colors.green,
            width: 150,
            height: 50,
            child: Text(
              "Saya sedang melatih kemampuan Flutter Saya",
              style: TextStyle(
                  color: Colors.white,
                  fontStyle: FontStyle.italic,
                  fontWeight: FontWeight.w700,
                  fontFamily: "Arial"),
            ),
          ),
        ),
      ),
    );
  }
}
