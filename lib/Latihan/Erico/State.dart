import 'package:flutter/material.dart';

class StateFull extends StatefulWidget {
  const StateFull({Key? key}) : super(key: key);

  @override
  _StateFullState createState() => _StateFullState();
}

class _StateFullState extends State<StateFull> {
  int number = 0;

  void tekanTombol() {
    setState(() {
      number = number + 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(number.toString()),
        ],
      ),
    );
  }
}
