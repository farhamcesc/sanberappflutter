import 'package:flutter/material.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ButtonStyle style =
        ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20));

    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 47.0),
        child: Column(
          children: [
            SizedBox(height: 49),
            Center(
              child: Text.rich(
                TextSpan(
                  text: "Sanber Flutter",
                  style: TextStyle(
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.bold,
                      fontSize: 30,
                      color: Colors.lightBlueAccent),
                ),
              ),
            ),
            SizedBox(height: 13),
            SizedBox(
              height: 93.8,
              width: 100,
              child: Image.asset('assets/img/flutter.png'),
            ),
            SizedBox(height: 29),
            TextField(
              decoration: InputDecoration(
                alignLabelWithHint: null,
                hintText: "Username",
                hintStyle: TextStyle(
                  fontFamily: "Poppins",
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
            ),
            SizedBox(height: 18.5),
            TextField(
              decoration: InputDecoration(
                alignLabelWithHint: null,
                hintText: "Password",
                hintStyle: TextStyle(fontFamily: "Poppins"),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
            ),
            SizedBox(height: 18.5),
            Center(
              child: Text.rich(
                TextSpan(
                  text: "Forgot Password",
                  style: TextStyle(
                      fontFamily: 'Poppins',
                      fontSize: 14,
                      color: Colors.lightBlueAccent),
                ),
              ),
            ),
            ElevatedButton(
              child: const Text('Login'),
              style: style,
              onPressed: () {},
            ),
            SizedBox(height: 50),
            SizedBox(
              child: Container(
                child: Align(
                  alignment: Alignment.bottomLeft,
                  child: Text.rich(
                    TextSpan(
                      text: "Does not have Acount?",
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 14,
                          color: Colors.lightBlueAccent),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(height: 15),
            SizedBox(
              height: 300,
              child: GridView.count(
                crossAxisCount: 2,
                padding: EdgeInsets.zero,
                childAspectRatio: 1.491,
                crossAxisSpacing: 10,
                mainAxisSpacing: 10,
                physics: NeverScrollableScrollPhysics(),
                children: [
                  Image.asset('assets/img/Monas.png'),
                  Image.asset('assets/img/Berlin.png'),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
