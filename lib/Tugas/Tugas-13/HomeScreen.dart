import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 42.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 30),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IconButton(onPressed: () {}, icon: Icon(Icons.notifications)),
                IconButton(
                    onPressed: () {},
                    icon: Image.asset('assets/img/add_shopping_cart.png')),
              ],
            ),
            SizedBox(height: 37),
            Text.rich(
              TextSpan(
                text: "Welcome, ",
                style: TextStyle(
                    fontFamily: 'Raleway',
                    fontWeight: FontWeight.bold,
                    fontSize: 40,
                    color: Colors.lightBlueAccent),
              ),
            ),
            Text.rich(
              TextSpan(
                text: "Farham Harvianto, ",
                style: TextStyle(
                    fontFamily: 'Poppins',
                    fontSize: 40,
                    color: Colors.blue.shade900),
              ),
            ),
            SizedBox(height: 130),
            TextField(
              decoration: InputDecoration(
                prefixIcon: Icon(Icons.search, size: 18),
                alignLabelWithHint: null,
                hintText: "Search",
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
            ),
            SizedBox(height: 112),
            Text(
              'Recommended Place',
              style: TextStyle(fontWeight: null, fontSize: 20),
            ),
            SizedBox(height: 20),
            SizedBox(
              height: 300,
              child: GridView.count(
                crossAxisCount: 2,
                padding: EdgeInsets.zero,
                childAspectRatio: 1.491,
                crossAxisSpacing: 10,
                mainAxisSpacing: 10,
                physics: NeverScrollableScrollPhysics(),
                children: [
                  for (var country in countries)
                    Image.asset('assets/img/$country.png')
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

final countries = ['Berlin', 'Roma', 'Monas', 'Tokyo'];
