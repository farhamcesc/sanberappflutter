import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
//import 'package:sanberappflutter/Tugas/Tugas-14/Models/UserModel.dart';
import 'package:sanberappflutter/Tugas/Tugas-14/PostData.dart';

class GetData extends StatefulWidget {
  @override
  _GetDataState createState() => _GetDataState();
}

class _GetDataState extends State<GetData> {
  final String url = "https://achmadhilmy-sanbercode.my.id/api/v1/profile";
  List? data;

  @override
  void initState() {
    super.initState();
    this.getJsonData();
  }

  Future<String?> getJsonData() async {
    http.Response response =
        await http.get(Uri.parse(url), headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = jsonDecode(response.body);
      data = convertDataToJson;
    });

    return "success";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueAccent,
      appBar: AppBar(
        title: Text("List Profile"),
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: Colors.white,
              ),
              onPressed: () {
                // Navigator.push(
                //   context,
                //   MaterialPageRoute(builder: (context) => PostData()),
                // );
              },
              child: Icon(
                Icons.add,
                size: 26.0,
                color: Colors.lightBlueAccent,
              ),
            ),
          )
        ],
      ),
      body: new ListView.builder(
        itemCount: data == null ? 0 : data!.length,
        itemBuilder: (BuildContext context, int index) {
          return new Container(
            margin: const EdgeInsets.all(5.0),
            child: new Center(
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  ElevatedButton(
                      onPressed: () {},
                      child: new Container(
                        child: new Text(data![index]['name'].toString()),
                        padding: const EdgeInsets.all(20.00),
                      ))
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Card buildCard(IconData icondData, String text) {
    return Card(
      elevation: 5,
      child: Row(
        children: <Widget>[
          Container(margin: EdgeInsets.all(5), child: Icon(icondData)),
          Text(text)
        ],
      ),
    );
  }
}
