import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class DrawerScreen extends StatefulWidget {
  @override
  _DrawwerScreenState createState() => _DrawwerScreenState();
}

class _DrawwerScreenState extends State<DrawerScreen> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: Text("Farham Harvianto"),
            currentAccountPicture: CircleAvatar(
              backgroundImage: AssetImage("assets/img/farham.png"),
            ),
            accountEmail: Text("Farhamapple@gmail.com"),
          ),
          DrawerListTile(
            icondata: Icons.group,
            title: "NewGroup",
            onTilePRessed: () {},
          ),
          DrawerListTile(
            icondata: Icons.lock,
            title: "New Secret Group",
            onTilePRessed: () {},
          ),
          DrawerListTile(
            icondata: Icons.notifications,
            title: "New Channel chat",
            onTilePRessed: () {},
          ),
          DrawerListTile(
            icondata: Icons.contacts,
            title: "Contacts",
            onTilePRessed: () {},
          ),
          DrawerListTile(
            icondata: Icons.bookmark_border,
            title: "Save Message",
            onTilePRessed: () {},
          ),
          DrawerListTile(
            icondata: Icons.phone,
            title: "Calls",
            onTilePRessed: () {},
          ),
        ],
      ),
    );
  }
}

class DrawerListTile extends StatelessWidget {
  final IconData? icondata;
  final String? title;
  final VoidCallback? onTilePRessed;

  const DrawerListTile(
      {Key? key, this.icondata, this.title, this.onTilePRessed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTilePRessed,
      dense: true,
      leading: Icon(icondata),
      title: Text(
        title!,
        style: TextStyle(fontSize: 16),
      ),
    );
  }
}
