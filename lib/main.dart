import 'package:flutter/material.dart';
//import 'package:sanberappflutter/Quiz3/LoginScreen.dart';
//import 'package:sanberappflutter/Tugas/Tugas-12/Telegram.dart';
import 'package:sanberappflutter/Tugas/Tugas-14/GetData.dart';
//import 'package:sanberappflutter/Tugas/Tugas-15/HomeScreen2.dart';
//import 'package:sanberappflutter/Tugas/Tugas-15/LoginScreen2.dart';
//import 'package:sanberappflutter/Quiz3/MainApp.dart';
//import 'package:sanberappflutter/Tugas/Tugas-14/GetData.dart';
//import 'package:sanberappflutter/Latihan/Erico/State.dart';
//import 'package:sanberappflutter/Latihan/Erico/TextWidget.dart';
//import 'package:sanberappflutter/Tugas/Tugas-13/HomeScreen.dart';
//import 'package:sanberappflutter/Tugas/Tugas-13/LoginScreen.dart';
//import 'package:sanberappflutter/Tugas/Tugas-15/LoginScreen2.dart';
//import 'package:sanberappflutter/Tugas/Tugas-15/HomeScreen2.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: GetData(),
      debugShowCheckedModeBanner: false,
    );
  }
}
